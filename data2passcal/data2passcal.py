#!/usr/bin/env python
'''
data2passcal

Ship miniseed data to passcal via ftp for qc before archival at DMC DMS
Lloyd Carothers IRIS/PASSCAL

Maeva Pourpoint
July 2020
Updates to work under Python 2 and 3.
Unit tests to ensure basic functionality.
Code cleanup to conform to the PEP8 style guide.
Directory cleanup (remove unused files introduced by Cookiecutter).
Packaged with conda.
'''
from __future__ import division, print_function

import datetime
import ftplib
import logging
import os
import pickle
import re
import signal
import socket
import struct
import sys

from io import open
from time import sleep, time

try:
    from urllib.request import urlopen
except ImportError:
    from urllib2 import urlopen

VERSION = '2023.2.0.0'


# FTP related
FTP_IP = '129.138.26.29'
FTP_HOST = 'qc.passcal.nmt.edu'
FTP_USER = 'ftp'
FTP_PASSWORD = 'data2passcal'
FTP_DIR = 'AUTO/MSEED'
FTP_TIMEOUT = 120
FTP_RECONNECT_WAIT = 60
FTP_BLOCKSIZE = 8192
# number of time to try to open the ftp connection
FTP_CONNECT_ATTEMPTS = 60 * 60 * 24 * 7 / FTP_RECONNECT_WAIT
# number of times a single file with try to be sent
FTP_SEND_ATTEMPTS = 3
# debug level of ftplib, 0-3
FTP_DEBUG_LEVEL = 0

# Files related
LOGFILE = 'data2passcal.log'
# store the files sent in ~/data2passcal.sent
SENTFILE = os.path.join(os.path.expanduser('~'), '.data2passcal.sent')
# If this file exists open it, incorporate and save to new name, and delete old
SENTFILE_OLD = os.path.join(os.path.expanduser('~'), '.send2passcal.sent')

HELP = '''
data2passcal
VERSION: %s
Usage: data2passcal dir
  data2passcal is a utility that sends day-long MSEED files ready for archival
  at the DMC to PASSCAL's QC system by:
  - Scanning all files below directory dir.
  - Filtering out non-miniseed files, by inspecting the first blockette of each
    file.
  - Sending the files to the automated system for ingestion into the QC system
    via ftp.
  You can send a SIGTERM (ctl-c) to data2passcal and it will shutdown cleanly.
  A list of sent files is kept in ~/.data2passcal.sent.
  Subsequent runs of send2passcal will not send files already sent.
  A log is stored in data2passcal.log in the current directory.
''' % VERSION

# Verbosity of console log from logging module from DEBUG to CRITICAL
DEBUG = logging.DEBUG

# Configure logging file and stdout
# todo if sending a file do we want to send all previous logs even if already
# sent
logger = logging.getLogger('__name__')
logger.setLevel(logging.DEBUG)
# Log to a file with debug level
logfh = logging.FileHandler(LOGFILE)
logfh.setLevel(logging.DEBUG)
logfh.setFormatter(logging.Formatter(
                   '%(asctime)s - %(levelname)s -\t%(message)s',
                   datefmt='%Y-%m-%d %H:%M:%S %z'))
logger.addHandler(logfh)
# Log to stdout
logconsole = logging.StreamHandler()
logconsole.setLevel(logging.INFO)
logconsole.setFormatter(logging.Formatter('%(message)s'))
logger.addHandler(logconsole)
logger.debug('Program starting.')
logger.debug('%s - v%s - Python:%s' % (sys.argv, VERSION, sys.version))
logger.debug('CWD: %s' % os.getcwd())
logger.info('Version: ' + VERSION)
logger.info('TIMEOUT: %d' % FTP_TIMEOUT)
logger.info('FTP RECONNECT WAIT: %d' % FTP_RECONNECT_WAIT)
# -- End logging


def scan_dir(dir):
    '''Returns a list of absolute file names found below root dir'''
    rootdir = dir
    logger.info('Scanning: %s' % os.path.abspath(dir))
    filelist = []
    filesize = 0
    foldercount = 0
    starttime = time()
    for root, subfolders, files in os.walk(rootdir):
        foldercount += len(subfolders)
        for file in files:
            f = os.path.abspath(os.path.join(root, file))
            try:
                filesize += os.path.getsize(f)
            except OSError:
                logger.exception('Can not stat %s' % f)
            else:
                filelist.append(f)
    logger.info('Total Size = %s' % format_size(filesize))
    logger.info('Total Files = %s' % len(filelist))
    logger.info('Total Dirs = %s' % foldercount)
    logger.info('Scan time = %0.2fs' % (time() - starttime))
    return filelist


def sendable(file):
    '''Filters files scanned returning a new list of files to send'''
    if os.path.basename(file).startswith('.'):
        return False
    if ismseed(file):
        return True
    return False


# Basic Miniseed file metadata extracted from filename
# This includes .p files which we may accept in the future
MseedRE = re.compile(r'\A(.*)\.([A-Z0-9][A-Z0-9])\.(.*)\.'
                     r'([A-Z][A-Z]\w)\.([0-9]{4})\.([0-9]{3})(?:\.p)*')


def filename_qc_format(file):
    '''Used to filter filename if correct for qc system'''
    return MseedRE.match(os.path.basename(file))


def format_size(num):
    '''Format bytes into human readble with suffix'''
    for suffix in ['bytes', 'KB', 'MB', 'GB']:
        if num < 1024.0:
            return '%3.3f %s' % (num, suffix)
        num /= 1024.0
    return '%3.3f %s' % (num, 'TB')


def ismseed(file):
    try:
        ms = open(file, 'rb')
    except Exception:
        logger.exception('Failed to open file %s in binary mode'
                         % os.path.basename(file))
        return None
    order = ByteOrder(ms)
    ms.close()
    del ms
    if order != "unknown":
        return True
    else:
        return False

#########################################################
# Taken from Bruces LibTrace


def ByteOrder(infile, seekval=20):
    """
    read file as if it is mseed just pulling time info
    from fixed header and determine if it makes sense unpacked
    as big endian or little endian
    """
    Order = "unknown"
    try:
        # seek to timeblock and read
        infile.seek(seekval)
        timeblock = infile.read(10)

        # assume big endian
        (Year, Day, Hour, Min, Sec, junk, Micro) =\
            struct.unpack('>HHBBBBH', timeblock)
        # test if big endian read makes sense
        if 1950 <= Year <= 2050 and \
           1 <= Day <= 366 and \
           0 <= Hour <= 23 and \
           0 <= Min <= 59 and \
           0 <= Sec <= 59:
            Order = "big"
        else:
            # try little endian read
            (Year, Day, Hour, Min, Sec, junk, Micro) =\
                struct.unpack('<HHBBBBH', timeblock)
            # test if little endian read makes sense
            if 1950 <= Year <= 2050 and \
               1 <= Day <= 366 and \
               0 <= Hour <= 23 and \
               0 <= Min <= 59 and \
               0 <= Sec <= 59:
                Order = "little"
    except Exception:
        logger.exception('Failed to read time info from fixed header')
        pass

    return Order
#########################################################


def get_sent_file_list(sentfile=SENTFILE):
    sentlist = []
    if os.path.isfile(sentfile):
        logger.debug('Using sentfile %s' % sentfile)
        if os.path.getsize(sentfile) > 0:
            with open(sentfile, 'rb') as f:
                sentlist = pickle.load(f)
    elif os.path.isfile(SENTFILE_OLD):
        logger.debug('Using old sentfile %s' % SENTFILE_OLD)
        if os.path.getsize(SENTFILE_OLD) > 0:
            with open(SENTFILE_OLD, 'rb') as f:
                sentlist = pickle.load(f)
    return sentlist


def write_sent_file_list(sentlist, sentfile=SENTFILE):
    logger.info('Saving list of files sent to passcal')
    with open(sentfile, 'wb+') as f:
        pickle.dump(sentlist, f, protocol=2)


def get_FTP():
    '''
    returns a FTP connection or None if a connection could not be made
    after number of attempts allowed
    '''
    trys = 0
    while trys < FTP_CONNECT_ATTEMPTS:
        trys += 1
        try:
            local_ip = socket.gethostbyname(socket.gethostname())
        except socket.gaierror:
            logger.exception('Valid address-to-host mapping does not exist')
            local_ip = ''
        logger.info('Connecting to FTP host %s from %s. Attempt %d of %d'
                    % (FTP_HOST,
                       local_ip,
                       trys,
                       FTP_CONNECT_ATTEMPTS))
        try:
            FTP = ftplib.FTP(host=FTP_HOST, user=FTP_USER,
                             passwd=FTP_PASSWORD, timeout=FTP_TIMEOUT)
            FTP.set_debuglevel(FTP_DEBUG_LEVEL)
            FTP.cwd(FTP_DIR)
            FTP.set_pasv(True)
        except (ftplib.all_errors + (AttributeError,)):
            logger.error('Failed to open FTP connection to %s' % FTP_HOST)
            test_network()
            logger.info('Waiting %.2f seconds before trying to reconnect...'
                        % FTP_RECONNECT_WAIT)
            sleep(FTP_RECONNECT_WAIT)
        else:
            logger.info('Success: Connected to PASSCAL FTP')
            return FTP
    logger.error('Giving up.')
    return None


def test_network():
    passcal_http_reachable()
    google_http_reachable()
    passcal_ftp_reachable()


def passcal_http_reachable():
    '''download and time passcal home page'''
    url = 'http://www.passcal.nmt.edu/'
    return url_reachable(url=url)


def google_http_reachable():
    '''download and time google home page'''
    url = 'http://www.google.com/'
    return url_reachable(url=url)


def passcal_ftp_reachable():
    '''Download a small file from passcals general ftp'''
    url = 'ftp://ftp.passcal.nmt.edu/download/public/DO_NOT_DELETE.dat'
    return url_reachable(url=url)


def url_reachable(url='http://www.passcal.nmt.edu/'):
    '''fetches a url returns True or False'''
    start = time()
    try:
        f = urlopen(url)
    except Exception:
        logger.exception("Failed to open connection to %s" % url)
        return False
    logger.info('connection made to %s in %f sec' % (url, time() - start))
    data = f.read()
    runtime = time() - start
    size = len(data)
    rate = size / runtime
    logger.info('%d B/sec in %f sec' % (rate, runtime))
    return True


def test_FTP(FTP):
    try:
        FTP.voidcmd('NOOP')
    except ftplib.all_errors:
        logger.error("Failed to send a simple command string to the server"
                     "and handle the response")
        return False
    except AssertionError:
        logger.error("Failed to send a simple command string to the server"
                     "and handle the response")
        return False
    else:
        return True


def send2passcal(mslist, sentlist=None):
    '''Send the list of files in mslist to passcal via FTP'''

    # Handle SIGINT while in this function: to gracefully close connection and
    # save sentlist to disk file
    def signal_handler(signum, frame):
        logger.info('Caught interrupt while FTPing. Aborting transfer %s'
                    % current_file)
        logger.info('Sent %d of %d' % (num_sent, num_to_send))
        logger.info('Sent %s of %s'
                    % (format_size(size_sent), format_size(size_to_send)))
        logger.info('%s /sec'
                    % (format_size(size_sent / (time() - starttime))))
        logger.info('Ran for %f sec' % (time() - starttime))
        write_sent_file_list(sentlist)
        try:
            if FTP:
                FTP.abort()
                FTP.quit()
        except Exception:
            logger.error("Failed to abort file transfer and close FTP "
                         "connection")
        os._exit(1)

    def update(data):
        '''Updates the terminal display.'''
        signal.signal(signal.SIGINT, signal_handler)
        update.bytes_sent += len(data)
        print('\r' + str(PB) + ' %s /sec '
              % (format_size(size_sent / (time() - starttime))), end=' ')
        '''
        print '%s %0.2f%%. %0.10d / %0.10d.'
              % (current_file.center(20),
                 (update.bytes_sent / update.file_size) * 100,
                 update.bytes_sent,
                 file_size)
        '''
        ETA_sec = ((time() - starttime) / size_sent) * \
            (size_to_send - size_sent)
        print('ETA %s %s %s' % (str(datetime.timedelta(seconds=(ETA_sec))),
                                current_file.center(20), ' ' * 20), end=' ')
        sys.stdout.flush()

    if sentlist is None:
        sentlist = []
    logger.info('Sending MSEED files to PASSCAL')
    num_to_send = len(mslist)
    num_sent = 0
    size_to_send = sum((os.path.getsize(f) for f in mslist))
    size_sent = 1
    current_file = ''
    signal.signal(signal.SIGINT, signal_handler)
    logger.info('Sending %d, %s files to PASSCAL' %
                (num_to_send, format_size(size_to_send)))
    FTP = get_FTP()
    starttime = time()
    PB = ProgressBar(num_to_send)
    for f in mslist:
        PB.update_time(num_sent)
        current_file = os.path.basename(f)
        trys = 0
        while trys < FTP_SEND_ATTEMPTS:
            trys += 1
            update.bytes_sent = 0
            fh = open(f, 'rb')
            file_size = os.path.getsize(f)
            update.file_size = float(file_size)
            try:
                FTP.storbinary('STOR %s' % current_file, fh,
                               blocksize=FTP_BLOCKSIZE, callback=update)
            except ftplib.error_perm:
                # This is permission and the error when 550 for the .in file
                # already exists so we should just continue with the next file
                # todo create a list of failed files and resend those at the
                # end instead of requiring a rerun
                logger.error('Failed to send file %s, permission error.'
                             % current_file)
                fh.close()
                break
            except (ftplib.all_errors + (AttributeError,)):
                # since we can restore with how we have proftp setup.
                # There is nothing more we can do with this file
                # Until the server rms the .in.file
                # , trys, FTP_SEND_ATTEMPTS)
                logger.error('Failed to send file %s.' % (current_file))
                # if DEBUG:
                # print "Waiting %d..." %FTP_TIMEOUT; sleep(FTP_TIMEOUT)
                try:
                    if FTP:
                        FTP.abort()
                        FTP.quit()
                except Exception:
                    logger.exception("Failed to abort file transfer and close"
                                     "FTP connection")
                    pass
                FTP = get_FTP()
                fh.close()
                if FTP is None:
                    signal_handler(None, None)
            else:
                fh.close()
                num_sent += 1
                logger.debug('Sent: %s' % f)
                size_sent += file_size
                sentlist.append(f)
                break

    logger.info('Sent %d of %d' % (num_sent, num_to_send))
    logger.info('Sent %s of %s'
                % (format_size(size_sent), format_size(size_to_send)))
    logger.info('%s /sec' % (format_size(size_sent / (time() - starttime))))
    logger.info('Ran for %f sec' % (time() - starttime))
    if FTP and test_FTP(FTP):
        FTP.quit()

########################################################
# From progressbar 3rd party class will eventually dump not awesome


class ProgressBar(object):
    def __init__(self, duration):
        self.duration = duration
        self.prog_bar = '[]'
        self.fill_char = '#'
        self.width = 40
        self.__update_amount(0)

    def animate(self):
        for i in range(self.duration):
            if sys.platform.lower().startswith('win'):
                print(self, '\r', end=' ')
            else:
                print(self, chr(27) + '[A')
            self.update_time(i + 1)
            time.sleep(1)
        print(self)

    def update_time(self, elapsed_secs):
        self.__update_amount((elapsed_secs / float(self.duration)) * 100.0)
        # self.prog_bar += '  %d/%s files' % (elapsed_secs, self.duration)

    def __update_amount(self, new_amount):
        percent_done = int(round((new_amount / 100.0) * 100.0))
        all_full = self.width - 2
        num_hashes = int(round((percent_done / 100.0) * all_full))
        self.prog_bar = '[' + self.fill_char * \
            num_hashes + ' ' * (all_full - num_hashes) + ']'
        pct_place = int((len(self.prog_bar) / 2) - len(str(percent_done)))
        pct_string = '%d%%' % percent_done
        self.prog_bar = self.prog_bar[0:pct_place] + \
            (pct_string + self.prog_bar[pct_place + len(pct_string):])

    def __str__(self):
        return str(self.prog_bar)

# End progressbar
########################################################


def main():
    if len(sys.argv) < 2 or sys.argv[1] in ['-h', '--help', '-?']:
        print(HELP)
        sys.exit()
    # find all files below dir
    filelist = scan_dir(sys.argv[1])
    # filter for files named as the qc system likes
    logger.info('Removing improperly named files.')
    msnamedlist = list(filter(filename_qc_format, filelist))
    logger.info('Properly named files files: %d' % len(msnamedlist))
    logger.info('Other files: %d' % (len(filelist) - len(msnamedlist)))
    logger.info('Removing files that are not Miniseed.')
    mslist = list(filter(sendable, msnamedlist))
    logger.info('MiniSEED files: %d' % len(mslist))
    logger.info('Properly named but not miniseed files: %d'
                % (len(msnamedlist) - len(mslist)))
    logger.info('Removing files already sent to PASSCAL')
    sentlist = get_sent_file_list()
    unsentms = [f for f in mslist if f not in sentlist]
    logger.info('%d miniSEED files have already been sent, not resending.'
                % (len(mslist) - len(unsentms)))
    send2passcal(unsentms, sentlist)
    write_sent_file_list(sentlist)


if __name__ == '__main__':
    main()
