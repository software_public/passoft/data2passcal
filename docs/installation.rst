.. highlight:: shell

============
Installation
============

From sources
------------

The sources for data2passcal can be downloaded from the `Github repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone https://git.passcal.nmt.edu/passoft/data2passcal

Or download the `tarball`_:

.. code-block:: console

    $ curl  -OL https://git.passcal.nmt.edu/passoft/data2passcal/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Github repo: https://git.passcal.nmt.edu/passoft/data2passcal
.. _tarball: https://git.passcal.nmt.edu/passoft/data2passcal/tarball/master
