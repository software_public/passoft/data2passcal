============
data2passcal
============

* Description: Send MSEED files ready for archival at the DMC to PASSCAL's QC system
               Only accept day-long MSEED files.

* Usage: data2passcal dir

* Free software: GNU General Public License v3 (GPLv3)
