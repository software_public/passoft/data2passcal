=======
History
=======

2018.171 (2018-06-20)
------------------

* First release on new build system.

2018.228 (2018-08-16)
------------------
* Updated to work with python 2 and 3

2020.093 (2020-04-02)
------------------
* Updated to work with python 2 and 3.
* Resources used: Python-Future project and  Futurize tool.
* Changes tested using unittest module on individual test methods(tests.test_data2passcal)
* Changes tested against real seismic dataset (AB.2002 - 2012)

2020.107 (2020-04-16)
------------------
* Added some unit tests to ensure basic functionality of data2passcal
* Created configuration file (.ini file) to store and access ftp and test related variables
* Updated list of platform specific dependencies to be installed when installing data2passcal in dev mode (see setup.py)
* Installed and tested data2passcal against Python2.7 and Python3.6 using tox
* Formatted Python code to conform to the PEP8 style guide (exception: E722, E712, E501)

2020.114 (2020-04-23)
------------------
* Created conda packages for "data2passcal" that can run on Python2.7 and Python3.6
=> To create package:
   - clone the "data2passcal" repo
   - "cd" in "data2passcal" directory
   - run "conda-build ."
=> To install locally with dependencies:
   - run "conda install -c ${CONDA_PREFIX}/conda-bld/ data2passcal"

2020.119 (2020-04-28)
------------------
* Created conda packages for "data2passcal" that can run on Python3.[5,7,8]
* Tested data2passcal against Python3.[5,7,8] using tox
* Updated .gitlab-ci.yml to run a linter and unit tests for Python2 and Python3.[5,6,7,8] in GitLab CI pipeline

2020.213 (2020-07-31)
------------------
* Updated meta.yaml to build architecture and platform independent package
* Updated mock unit tests
